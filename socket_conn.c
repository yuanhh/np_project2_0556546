#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>
#include "socket_conn.h"

void sigchld_handler(int signal)
{
    while(waitpid(-1, NULL, WNOHANG) > 0);
}
void set_nonblock(int fd)
{
    int flag;
    int x;
    x = fcntl(fd, F_GETFL, &flag);
    if (x < 0) {
        //syslog(LOG_ERR, "fcntl F_GETFL: FD %d: %s", fd, strerror(errno));
        exit(1);
    }
    flag |= O_NONBLOCK;
    x = fcntl(fd, F_SETFL, &flag);
    if (x < 0) {
        //syslog(LOG_ERR, "fcntl F_SETFL: FD %d: %s", fd, strerror(errno));
        exit(1);
    }
}
int wait_for_connection(int fd)
{
    int conn_fd;
    socklen_t len = sizeof(struct sockaddr_in);
    static struct sockaddr_in peer;

    memset(&peer, 0, len);
    conn_fd = accept(fd, (struct sockaddr *)&peer, &len);

    if (conn_fd < 0)
    {
        if (errno != EINTR)
            perror("accept");
    }
    //set_nonblock(conn_fd);
    return conn_fd;
}
char *get_ip_str(const struct sockaddr *sa, char *s, size_t maxlen)
{
    switch(sa->sa_family) {
        case AF_INET:
            inet_ntop(AF_INET, &(((struct sockaddr_in *)sa)->sin_addr),
                    s, maxlen);
            break;
        case AF_INET6:
            inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)sa)->sin6_addr),
                    s, maxlen);
            break;
        default:
            strncpy(s, "Unknown AF", maxlen);
            return NULL;
    }
    return s;
}
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
int passive_socket(char *service, char *protocol, char *localip)
{
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    struct sigaction sa;
    int rv;
    int opt = 1;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    if (strncmp(protocol, "udp", 3) == 0)
    {
        hints.ai_socktype = SOCK_DGRAM;
    }
    else
    {
        hints.ai_socktype = SOCK_STREAM;
    }
    if (localip == 0)
    {
        hints.ai_flags = AI_PASSIVE;
        if ((rv = getaddrinfo(NULL, service, &hints, &servinfo)) != 0)
        {
            fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
            return 1;
        }
    }
    else
    {
        if ((rv = getaddrinfo(localip, service, &hints, &servinfo)) != 0)
        {
            fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
            return 1;
        }
    }
    for(p = servinfo; p != NULL; p = p->ai_next)
    {
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
        {
            perror("server: socket");
            continue;
        }
        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) == -1)
        {
            perror("setsockopt");
            exit(1);
        }
        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1)
        {
            close(sockfd);
            perror("server: bind");
            continue;
        }
        break;
    }
    if (p == NULL)
    {
        fprintf(stderr, "server: failed to bind\n");
        return 2;
    }
    freeaddrinfo(servinfo);

    if ((listen(sockfd, BACKLOG)) == -1)
    {
        perror("listen");
        exit(1);
    }
    sa.sa_handler = sigchld_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;

    if (sigaction(SIGCHLD, &sa, NULL) == -1)
    {
        perror("sigaction");
        exit(1);
    }
    return sockfd;
}
int connect_socket(char *host, char *service, char *protocol)
{
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    if (strncmp(protocol, "udp", 3) == 0)
    {
        hints.ai_socktype = SOCK_DGRAM;
    }
    else
    {
        hints.ai_socktype = SOCK_STREAM;
    }
    if ((rv = getaddrinfo(host, service, &hints, &servinfo)) != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }
    for(p = servinfo; p != NULL; p = p->ai_next)
    {
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
        {
            perror("client: socket");
            continue;
        }

        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1)
        {
            close(sockfd);
            perror("client: connect");
            continue;
        }
        break;
    }
    if (p == NULL)
    {
        fprintf(stderr, "client: failed to connect\n");
        return 2;
    }
    freeaddrinfo(servinfo);

    return sockfd;
}
