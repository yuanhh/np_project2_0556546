#ifndef SOCKET_CONN_H
#define SOCKET_CONN_H

#include<sys/socket.h>
#define BACKLOG 30

typedef unsigned char byte;
typedef unsigned short int word16;
typedef unsigned int word32;

void sigchld_handler(int signal);

void set_nonblock(int fd);

int wait_for_connection(int fd);

char *get_ip_str(const struct sockaddr *sa, char *s, size_t maxlen);

void *get_in_addr(struct sockaddr *sa);

int passive_socket(char *service, char *protocol, char *localip);

int connect_socket(char *host, char *service, char *protocol);

int wait_for_connection(int fd);
#endif
