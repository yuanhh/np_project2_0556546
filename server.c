#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netdb.h>
#include<netinet/in.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<ctype.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<sys/sem.h>
#include<arpa/inet.h>
#include<semaphore.h>
#include"rwg_service.h"
#include"socket_conn.h"

#define RWG_PORT "7002"

int shm_table;
sem_t sem;

static void signal_handler(int signal)
{
    switch (signal) {
        case SIGUSR1:
            receive_msg(getpid());
            break;
        case SIGINT:
            delete_client(getpid());
            exit(0);
            break;
        default:
            break;
    }
}

int main( int argc, char *argv[] )
{
    int listen_fd = 0, conn_fd = 0;
    int client_id = 0;

    signal(SIGUSR1, signal_handler);
    signal(SIGINT, signal_handler);

    sem_init(&sem, 0, 1);

    shm_table = shmget(IPC_PRIVATE, sizeof(int) * MAX_CLIENT, IPC_CREAT|PERMS);
    if (shm_table == -1) perror("ERROR on shmget.\r\n");
    shmid_table_initialize();

    switch (argc)
    {
        case 1:
            listen_fd = passive_socket(RWG_PORT, "tcp", 0);
            break;
        case 2:
            listen_fd = passive_socket(argv[1], "tcp", 0);
            break;
        default:
            fprintf(stderr, "Usage: %s <port>\n", argv[0]);
            exit(1);
    }

    while (1)
    {
        struct sockaddr_in client_addr;
        socklen_t client_len = sizeof(client_addr);
        conn_fd = accept(listen_fd, (struct sockaddr*) &client_addr, &client_len);
        if (client_id == -1 || conn_fd < 0)\
        {
            fprintf(stderr, "Server is full loaded.\n");
            continue;
        }
        int childpid = 0;
        if ((childpid = fork()) < 0)
        {
            fputs("ERROR on fork.\n", stderr);
        }
        else if (childpid == 0)
        {
            close(listen_fd);
            dup2(conn_fd, 1);
            dup2(conn_fd, 2);
            char client_ip[INET_ADDR_LEN] = {'\0'};
            inet_ntop(AF_INET, &(client_addr.sin_addr), client_ip, INET_ADDR_LEN);
            sem_wait(&sem);
            new_client(conn_fd, getpid(), client_ip, ntohs(client_addr.sin_port));
            sem_post(&sem);
            rwg_service(shm_table);
            delete_client(getpid());
            close(conn_fd);
            exit(0);
        }
        else
        {
            close(conn_fd);
        }
    }
    close(listen_fd);
    user *shm_addr = (user *)shmat(shm_table, NULL, 0);
    shmdt(shm_addr);
    sem_destroy(&sem);
    return 0;
}
