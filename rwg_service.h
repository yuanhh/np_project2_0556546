#ifndef RWG_SERVICE_H
#define RWG_SERVICE_H

#define PROMPT "% "
#define CMDSIZE 257
#define BUFFERSIZE 15001
#define CLIENT_STDIN 0
#define CLIENT_STDOUT 1
#define CLIENT_STDERR 2
#define INET_ADDR_LEN 16
#define MAX_CLIENT 31
#define MAX_MSG_SIZE 11
#define MAX_MSG_LEN 1024
#define SEMKEY 123456L
#define FIFO_PATH "/tmp"
#define PERMS 0666

typedef struct User {
    int sock_fd;
    int client_id;
    int pid;
    char name[21];
    char ip[INET_ADDR_LEN];
    int port;
    char msg_box[MAX_MSG_SIZE][MAX_MSG_LEN];
    int q_front;
    int q_rear;
    int fifo_readfd[MAX_CLIENT];
    char fifo_readcmd[MAX_CLIENT][CMDSIZE];
} user;

int receive_msg(int pid);
int send_msg(int shmid, char *msg);
void broadcast(char *msg);
void shmid_table_initialize();
int find_empty_service();
int new_client(int conn_fd, int pid, char *client_ip, int client_port);
int delete_client(int pid);
int name(int pid, char *name);
int tell(int pid, int client_id, char *msg);
int yell(int pid, char *msg);
int who(int pid);
int find_empty_entry(const int pipe_number_entry[1000][2]);
int read_to_new_line(int pipe_number_entry[1000][2]);
int readline(int fd, char* cmd, int maxlen);
char** cmdtok(char *cmds_in_line, const char delim, int *cmds_count);
int file_io(const char *cmds, char **args, int *arg_count, int *to_file, int *from_file);
void free_2Darray(char **array, int count);
int pipe_to_next_line(char *last_arg, int *pipe_number);
int exec_shell_command(char *args[], int is_piped_cmd, int cmds_count, int *pipe_read_fd, int to_file, int from_file, int flag);
int rwg_service(int);
#endif
