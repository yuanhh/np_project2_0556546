#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<ctype.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<errno.h>
#include<semaphore.h>
#include"rwg_service.h"

extern int shm_table;

int find_shmid_of_pid(int pid)
{
	int *shmid_table = shmat(shm_table, NULL, 0);
	int i = 0;

	for (i = 1; i < MAX_CLIENT; i++) {
		if (shmid_table[i] != 0) {
			user *tmp = (user *)shmat(shmid_table[i], NULL, 0);
			if (tmp->pid == pid)
				return tmp->client_id;
		}
	}
	return -1;
}
int receive_msg(int pid)
{
	int *shmid_table = shmat(shm_table, NULL, 0);
	int id = find_shmid_of_pid(getpid());
	user *tmp = (user *)shmat(shmid_table[id], NULL, 0);
	while (tmp->q_rear != -1 || tmp->q_front != -1) {
		write(CLIENT_STDOUT, tmp->msg_box[tmp->q_front], strlen(tmp->msg_box[tmp->q_front]));
		memset(tmp->msg_box[tmp->q_front], 0, MAX_MSG_LEN);
		tmp->q_front = tmp->q_front + 1;
		if (tmp->q_front == MAX_MSG_SIZE)
			tmp->q_front = 0;
		if (tmp->q_front - 1 == tmp->q_rear) {
			tmp->q_front = -1;
			tmp->q_rear = -1;
		}
	}
	return 0;
}
int send_msg(int shmid, char *msg)
{
	user *tmp = (user *)shmat(shmid, NULL, 0);
	int write_allocate = (tmp->q_rear + 1) % MAX_MSG_SIZE;

	if ((tmp->q_front == 0 && tmp->q_rear == MAX_MSG_SIZE - 1) || (write_allocate == tmp->q_front)) {
		perror("Message buffer is full.\n");
	} else {
		if (tmp->q_rear == MAX_MSG_SIZE - 1 && tmp->q_front != 0)
			tmp->q_rear = -1;
		strncpy(tmp->msg_box[write_allocate], msg, MAX_MSG_LEN);
		tmp->q_rear = write_allocate;
		if (tmp->q_front == -1)
			tmp->q_front = 0;
		kill(tmp->pid, SIGUSR1);
	}
	return 0;
}
void broadcast(char *msg)
{
	int *shmid_table = shmat(shm_table, NULL, 0);
	int i = 0;
	for (i = 1; i < MAX_CLIENT; i++) {
		if (shmid_table[i] != 0) {
			send_msg(shmid_table[i], msg);
		}
	}
}
void shmid_table_initialize()
{
	int *shmid_table = (int *)shmat(shm_table, NULL, 0);
	int i = 0;

	for (i = 0; i < MAX_CLIENT; i++) {
		shmid_table[i] = 0;
	}
}
int find_empty_service()
{
	int *shmid_table = (int *)shmat(shm_table, NULL, 0);
	int i = 0;
	for (i = 1; i < MAX_CLIENT; i++) {
		if (shmid_table[i] == 0)
			return i;
	}
	return -1;
}
int new_client(int conn_fd, int pid, char *client_ip, int client_port)
{
	const char Welcome_message[3][42] =
	{
		{"****************************************\r\n"},
		{"** Welcome to the information server. **\r\n"},
		{"****************************************\r\n"}
	};
	int *shmid_table = (int *)shmat(shm_table, NULL, 0);

	write(CLIENT_STDOUT, Welcome_message, strlen(*Welcome_message));

	int client_id;
	char msg[MAX_MSG_LEN] = {'\0'};
	//snprintf(msg, MAX_MSG_LEN, "*** User '(no name)' entered from %s/%d. ***\r\n", client_ip, client_port);
	snprintf(msg, MAX_MSG_LEN, "*** User '(no name)' entered from CGILAB/511. ***\n");


	client_id = find_empty_service();
	shmid_table[client_id] = shmget(IPC_PRIVATE, sizeof(user), IPC_CREAT|PERMS);
	user *shm_addr = (user *)shmat(shmid_table[client_id], NULL, 0);
	memset(shm_addr, 0, sizeof(user));
	shm_addr->sock_fd = conn_fd;
	shm_addr->client_id = client_id;
	shm_addr->pid = pid;
	shm_addr->port = client_port;
	strncpy(shm_addr->ip, client_ip, INET_ADDR_LEN);
	strncpy(shm_addr->name, "(no name)", 9);
	shm_addr->q_front = -1;
	shm_addr->q_rear = -1;

	int i = 0;
	for (i = 1; i < MAX_CLIENT; i++) {
		char fifo_path[50] = {'\0'};
		snprintf(fifo_path, 50, "%s/%d_to_%d.fifo", FIFO_PATH, i, client_id);
		if (mkfifo(fifo_path, PERMS) == -1)
		{
			if (errno != EEXIST)
				perror("can't create FIFO\n");
		}
		shm_addr->fifo_readfd[i] = open(fifo_path, O_RDONLY | O_NONBLOCK);
		memset(shm_addr->fifo_readcmd[i], '\0', CMDSIZE);
	}
	broadcast(msg);
	return 0;
}
int delete_client(int pid)
{
	int *shmid_table = (int *)shmat(shm_table, NULL, 0);
	int id = find_shmid_of_pid(pid);

	if (id == -1) return 0;

	char msg[MAX_MSG_LEN] = {'\0'};

	user *tmp = (user *)shmat(shmid_table[id], NULL, 0);

	snprintf(msg, MAX_MSG_LEN, "*** User '%s' left. ***\n", tmp->name);
	broadcast(msg);

	int i = 0;
	for (i = 1; i < MAX_CLIENT; i++) {
		char fifo_path[50] = {'\0'};
		snprintf(fifo_path, 50, "%s/%d_to_%d.fifo", FIFO_PATH, i, id);
		close(tmp->fifo_readfd[i]);
		unlink(fifo_path);
	}
	shmdt(tmp);
	shmid_table[id] = 0;

	return 0;
}
int name(int pid, char *name)
{
	int *shmid_table = (int *)shmat(shm_table, NULL, 0);
	int id = find_shmid_of_pid(pid);
	int i = 0;

	for (i = 1; i < MAX_CLIENT; i++) {
		if (shmid_table[i] != 0) {
			user *tmp = (user *)shmat(shmid_table[i], NULL, 0);
			if (strcmp(tmp->name, name) == 0)
				return -1;
		}
	} 
	user *tmp = (user *)shmat(shmid_table[id], NULL, 0);
	memset(tmp->name, 0, 21);
	strncpy(tmp->name, name, strlen(name));
	char msg[MAX_MSG_LEN] = {'\0'};
	//snprintf(msg, MAX_MSG_LEN, "*** User from %s/%d is named '%s'. ***\r\n", tmp->ip, tmp->port, tmp->name);
	snprintf(msg, MAX_MSG_LEN, "*** User from CGILAB/511 is named '%s'. ***\n", tmp->name);
	broadcast(msg);

	return 0;
}
int tell(int pid, int client_id, char *msg)
{
	int *shmid_table = (int *)shmat(shm_table, NULL, 0);
	int id = find_shmid_of_pid(pid);

	if (shmid_table[client_id] == 0)
		return -1;

	user *sender = (user *)shmat(shmid_table[id], NULL, 0);

	char str[MAX_MSG_LEN] = {'\0'};
	snprintf(str, MAX_MSG_LEN, "*** %s told you ***: %s\n", sender->name, msg);
	send_msg(shmid_table[client_id], str);

	return 0;
}
int yell(int pid, char *msg)
{
	int *shmid_table = (int *)shmat(shm_table, NULL, 0);
	int id = find_shmid_of_pid(pid);

	user *tmp = (user *)shmat(shmid_table[id], NULL, 0);

	char str[MAX_MSG_LEN] = {'\0'};
	snprintf(str, MAX_MSG_LEN, "*** %s yelled ***: %s\n", tmp->name, msg);

	broadcast(str);
}
int who(int pid)
{
	int *shmid_table = (int *)shmat(shm_table, NULL, 0);
	int id = find_shmid_of_pid(pid);
	int i = 0;

	send_msg(shmid_table[id], "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n");
	for (i = 1; i < MAX_CLIENT; i++) {
		if (shmid_table[i] != 0) {
			user *tmp = (user *)shmat(shmid_table[i], NULL, 0);
			char msg[MAX_MSG_LEN] = {'\0'};
			if (tmp->pid == pid) {
				//snprintf(msg, MAX_MSG_LEN, "%d\t%s\t%s/%d\t<- me\r\n", i, tmp->name, tmp->ip, tmp->port);
				snprintf(msg, MAX_MSG_LEN, "%d\t%s\tCGILAB/511\t<- me\n", i, tmp->name);
			} else {
				//snprintf(msg, MAX_MSG_LEN, "%d\t%s\t%s/%d\t\r\n", i, tmp->name, tmp->ip, tmp->port);
				snprintf(msg, MAX_MSG_LEN, "%d\t%s\tCGILAB/511\t\n", i, tmp->name);
			}
			send_msg(shmid_table[id], msg);
		}
	}
	return 0;
}
int find_empty_entry(const int pipe_number_entry[1000][2])
{
	int i;
	for (i = 0; i < 1000; i++) {
		if (pipe_number_entry[i][1] == 0)
			return i;
	}
	return -1;
}
int read_to_new_line(int pipe_number_entry[1000][2])
{
	int pipefd[2];
	int i = 0;
	int flag = 0;
	char buffer[BUFFERSIZE] = {'\0'};
	if ((pipe(pipefd)) == -1) {
		perror("ERROR on pipe.\n");
	}
	for (i = 0; i < 1000; i++) {
		if (pipe_number_entry[i][1] > 0) {
			if (pipe_number_entry[i][1] == 1) {
				flag = 1;
				read(pipe_number_entry[i][0], buffer, BUFFERSIZE);
				close(pipe_number_entry[i][0]);
				pipe_number_entry[i][0] = 0;
				write(pipefd[1], buffer, strlen(buffer));
			}
			pipe_number_entry[i][1]--;
		}
		memset(buffer, '\0', BUFFERSIZE);
	}
	close(pipefd[1]);
	if (flag == 1) {
		return pipefd[0];
	} else {
		close(pipefd[0]);
		return -1;
	}
}
int readline(int fd, char* cmd, int maxlen)
{
	int checked = 0, i = 0;
	char c = '\0';
	int wc = 0;

	for (i = 1; i < maxlen; i++) {
		if ((checked = read(fd, &c, 1)) == 1) {
			if (c == '\n') {
				i--;
				break;
			} else if (c == '\r') {
				i--;
				continue;
			}
			*cmd++ = c;
			wc++;
		} else if (checked == 0) {
			break;
		} else return -1;
	}
	*cmd = 0;
	return (wc);
}
char** cmdtok(char *cmds_in_line, const char delim, int *cmds_count)
{
	char **result = 0;
	char *cptr = cmds_in_line;
	char *token = 0;
	char *rest_of_cmd = NULL;
	int index = 0;
	int flag = 0;

	*cmds_count = 0;

	while (*cptr) {
		if ((*cptr != delim) && (flag == 0)) {
			(*cmds_count)++;
			flag = 1;
		} else if ((*cptr == delim) && (flag == 1)) {
			flag = 0;
		}
		cptr++;
	}
	while (result == NULL) {
		result = malloc(sizeof(char*) * ((*cmds_count) + 1));
	}
	token = strtok_r(cmds_in_line, &delim, &rest_of_cmd);

	for (index = 0; index < *cmds_count; index ++) {
		result[index] = strdup(token);
		token = strtok_r(rest_of_cmd, &delim, &rest_of_cmd); 
	}
	result[index] = NULL;

	return result;
}
int file_io(const char *cmds, char **args, int *arg_count, int *to_file, int *from_file)
{
	int i = 0;
	int *shmid_table = (int *)shmat(shm_table, NULL, 0);
	int id = find_shmid_of_pid(getpid());
	user *me = (user *)shmat(shmid_table[id], NULL, 0);
	for (i = 0; i < *arg_count; i++) {
		if (args[i][0] == '<') {
			int target_id = atoi(&args[i][1]);
			char msg[BUFFERSIZE] = {'\0'};
			if (shmid_table[target_id] == 0 || strlen(me->fifo_readcmd[target_id]) == 0) {
				snprintf(msg, 100, "*** Error: the pipe #%d->#%d does not exist yet. ***\n", target_id, id);
				write(CLIENT_STDOUT, msg, strlen(msg));
				return -1;
			} else {
				user *target = (user *)shmat(shmid_table[target_id], NULL, 0);
				*from_file = me->fifo_readfd[target_id];
				snprintf(msg, 100, "*** %s (#%d) just received from %s (#%d) by '%s' ***\n", me->name, id, target->name, target_id, cmds);
				broadcast(msg);
				memset(me->fifo_readcmd[target_id], '\0', CMDSIZE);
				free(args[i]);
				args[i] = strdup(args[*arg_count -1]);
				free(args[*arg_count - 1]);
				args[*arg_count - 1] = 0;
				*arg_count -= 1;
				i--;
			}
		}
	}
	for (i = 0; i < *arg_count; i++) {
		if (args[i][0] == '>') {
			if (isdigit(args[i][1])) {
				int target_id = atoi(&args[i][1]);
				if (shmid_table[target_id] != 0) {
					char msg[BUFFERSIZE] = {'\0'};
					char fifo_path[50] = {'\0'};
					user *target = (user *)shmat(shmid_table[target_id], NULL, 0);
					snprintf(fifo_path, 50, "%s/%d_to_%d.fifo", FIFO_PATH, id, target_id);
					if (strlen(target->fifo_readcmd[id]) != 0) {
						snprintf(msg, 100, "*** Error: the pipe #%d->#%d already exists. ***\n", id, target_id);
						write(CLIENT_STDOUT, msg, strlen(msg));
						return -1;
					}
					*to_file = open(fifo_path, O_WRONLY);
					snprintf(msg, 100, "*** %s (#%d) just piped '%s' to %s (#%d) ***\n", me->name, id, cmds, target->name, target_id);
					strncpy(target->fifo_readcmd[id], cmds, strlen(cmds));
					broadcast(msg);
				} else {
					char msg[50] = {'\0'};
					snprintf(msg, 50, "*** Error: user #%d does not exist yet. ***\n", target_id);
					write(CLIENT_STDOUT, msg, strlen(msg));
					return -1;
				}
				free(args[i]);
				args[i] = strdup(args[*arg_count -1]);
				free(args[*arg_count - 1]);
				args[*arg_count - 1] = 0;
				*arg_count -= 1;
				i--;
			} else {
				FILE *fp = fopen(args[i + 1], "w");
				if (fp) {
					*to_file = fileno(fp);
				} else {
					perror("ERROR on open file\n");
				}
				args[i] = 0;
				args[i + 1] = 0;
				*arg_count -= 2;
				return 0;
			}
		}
	}
	return 0;
}
void free_2Darray(char **array, int count)
{
	int i = 0;
	for (i = 0; i < count; i++) {
		if (array[i]) free(array[i]);
	}
	free(array);
}
int pipe_to_next_line(char *last_arg, int *pipe_number)
{
	char *tmp = (last_arg + 1);
	if (last_arg[0] == '?') {
		*pipe_number = atoi(tmp);
		return 1;
	} else if (last_arg[0] == '!') {
		*pipe_number = atoi(tmp);
		return 2;
	} else {
		return 0;
	}
}
int exec_shell_command(char *args[], int is_piped_cmd, int cmds_count, int *pipe_read_fd, int to_file, int from_file, int flag)
{
	pid_t childpid = 0;
	int status = 0;
	int pipefd[2];

	if (pipe(pipefd) == -1) {
		perror("ERROR on pipe.\n");
	}
	if ((childpid = fork()) < 0) {
		perror("ERROR on fork.\n");
		exit(-1);
	} else if (childpid == 0) {
		close(pipefd[0]);
		dup2(*pipe_read_fd, CLIENT_STDIN);
		if (from_file != -1) {
			dup2(from_file, CLIENT_STDIN);
		}
		if (to_file != -1) {
			dup2(to_file, CLIENT_STDOUT);
		}  else if (is_piped_cmd < (cmds_count - 1)) {
			dup2(pipefd[1], CLIENT_STDOUT);
		} else if (flag == 1) {
			dup2(pipefd[1], CLIENT_STDOUT);
		} else if (flag == 2) {
			dup2(pipefd[1], CLIENT_STDOUT);
			dup2(pipefd[1], CLIENT_STDERR);
		} 
		execvp(args[0], &args[0]);
		fprintf(stderr, "Unknown command: [%s].\n", args[0]);
		exit(-1);
	} else {
		wait(&status);
		if (*pipe_read_fd >= 0) {
			close(*pipe_read_fd);
		}
		*pipe_read_fd = 0;
		*pipe_read_fd = pipefd[0];
		close(pipefd[1]);
	}
	if (to_file != -1) close(to_file);
	return status;
}
int rwg_service(int shm)
{
	char cmds[BUFFERSIZE] = {'\0'};
	char original_cmd[BUFFERSIZE] = {'\0'};
	char **cmd_split= NULL;
	int cmdline_len = 0;
	int cmds_count = 0;
	int i = 0;
	int stop = 0;
	int to_file = 0;
	int from_file = 0;
	int pipe_read_fd = -1;
	int arg_count = 0;
	char **arg_split = 0;
	int pipe_number_flag = 0;
	int pipe_number = 0;
	int status = 0;
	int index = 0;
	int pipe_number_entry[1000][2] = {0};

	char cwd[100] = {'\0'};

	getcwd(cwd, sizeof(cwd)); 
	strcat(cwd, "/rwg/");

	chroot(cwd);
	chdir(cwd);
	putenv("PATH=bin:.");

	while (stop != 1)
	{
		write(CLIENT_STDOUT, PROMPT, strlen(PROMPT));
		memset(cmds, '\0', BUFFERSIZE);
		memset(original_cmd, '\0', BUFFERSIZE);
		cmdline_len = 0;
		cmdline_len = readline(CLIENT_STDOUT, cmds, BUFFERSIZE);
		strncpy(original_cmd, cmds, strlen(cmds));
		if (cmdline_len == 0) continue;

		//messanger setting commands
		if (strncmp(cmds, "exit", 4) == 0) break;
		else if (strncmp(cmds, "name", 4) == 0)
		{
			cmd_split = cmdtok(cmds, ' ', &cmds_count);
			if (name(getpid(), cmd_split[1]) == -1) {
				char msg[MAX_MSG_LEN] = {'\0'};
				snprintf(msg, MAX_MSG_LEN, "*** User '%s' already exists. ***\n", cmd_split[1]);
				write(CLIENT_STDOUT, msg, strlen(msg));
			}
			free_2Darray(cmd_split, cmds_count);
			continue;
		} 
		else if (strncmp(cmds, "tell", 4) == 0)
		{
			cmd_split = cmdtok(cmds, ' ', &cmds_count);
			int target_id = atoi(cmd_split[1]);
			char massage[BUFFERSIZE] = {'\0'};
			for (i = 2; i < cmds_count; i++) {
				strncat(massage, cmd_split[i], strlen(cmd_split[i]));
				strncat(massage, " ", 1);
			}
			if (tell(getpid(), target_id, massage) == -1) {
				char msg[MAX_MSG_LEN] = {'\0'};
				snprintf(msg, MAX_MSG_LEN, "*** Error: user #%d does not exist yet. ***\n", target_id);
				write(CLIENT_STDOUT, msg, strlen(msg));
			}
			free_2Darray(cmd_split, cmds_count);
			continue;
		} 
		else if (strncmp(cmds, "yell", 4) == 0)
		{
			yell(getpid(), &cmds[5]);
			continue;
		} 
		else if (strncmp(cmds, "who", 3) == 0)
		{
			who(getpid());
			continue;
		}
		//token shell commands
		for (i = 0; i < cmdline_len; i++) {
			if (cmds[i] == '|') {
				if (((i + 1) < cmdline_len) && isdigit(cmds[i + 1])) {
					cmds[i] = '?';
				}
			}
		}
		cmd_split = cmdtok(cmds, '|', &cmds_count);
		pipe_read_fd = -1;
		pipe_read_fd = read_to_new_line(pipe_number_entry);
		for (i = 0; i < cmds_count; i++) {
			arg_split = cmdtok(cmd_split[i], ' ', &arg_count);
			to_file = -1;
			from_file = -1;
			if (file_io(original_cmd, arg_split, &arg_count, &to_file, &from_file) == -1) break;
			pipe_number_flag = pipe_to_next_line(arg_split[arg_count - 1], &pipe_number);
			if (pipe_number_flag != 0)
				arg_split[arg_count - 1] = 0;
			if (strncmp(arg_split[0], "setenv", 6) == 0) {
				setenv(arg_split[1], arg_split[2], 1);
			} else if (strncmp(arg_split[0], "printenv", 8) == 0) {
				char msg[BUFFERSIZE] = {'\0'};
				snprintf(msg, BUFFERSIZE, "PATH=%s\n", getenv(arg_split[1]));
				write(CLIENT_STDOUT, msg, strlen(msg));
			} else {
				status = exec_shell_command(arg_split, i, cmds_count, &pipe_read_fd, to_file, from_file, pipe_number_flag);
				if (status != 0) break;
				if ((i == (cmds_count - 1)) && (pipe_number_flag != 0)) {
					index = find_empty_entry(pipe_number_entry);
					if (index < 0) {
						perror("pipe_number_table is full.\n");
						break;
					}
					pipe_number_entry[index][0] = pipe_read_fd;
					pipe_number_entry[index][1] = pipe_number;
					index = 0;
					pipe_number_flag = 0;
				}
			}
			free_2Darray(arg_split, arg_count);
		}
		free_2Darray(cmd_split, cmds_count);
	}
	kill(getppid(), SIGCHLD);
	return 0;
}
