
CC := gcc -pthread
CFLAGS := -g -Wall

rwg_server: server.o rwg_service.o socket_conn.o
	$(CC) $(CFLAG)  server.o rwg_service.o socket_conn.o -o rwg_server

rwg_service.o: rwg_service.c rwg_service.h
	$(CC) $(CFLAG) -c rwg_service.c

server.o: server.c rwg_service.h  socket_conn.h
	$(CC) $(CFLAG) -c server.c

socket_conn.o: socket_conn.c socket_conn.h
	 $(CC) $(CFLAG) -c socket_conn.c


.PHONY : clean
clean:
	-rm -f *.o rwg_server
