#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netdb.h>
#include<netinet/in.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<ctype.h>
#include<sys/ipc.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<arpa/inet.h>
#include<errno.h>
#include"socket_conn.h"

#define STDIN 0
#define STDOUT 1
#define STDERR 2
#define BUFFERSIZE 15001
#define CMDSIZE 257
#define RWG_PORT "7000"
#define WORKPWD "/home/yuanhao/np/np_project2_0556546/rwg/"
#define PROMPT "% "
#define MAX_CLIENT 31
#define MAX_MSG_LEN 1024
#define INET_ADDR_LEN 16
#define ENV_PATH "bin:."
#define FIFO_PATH "/tmp"
#define PERMS 0666

typedef struct User {
    int sock_fd;
    char *name;
    char ip[INET_ADDR_LEN];
    unsigned int port;
    char env[BUFFERSIZE];
    int fifo_readfd[MAX_CLIENT];
    char fifo_readcmd[MAX_CLIENT][CMDSIZE];
    int pipe_number_entry[1000][2];
} User;

User client[MAX_CLIENT];

int send_msg(int client_id, const char *msg);
int read_line(int fd, void *buffer, size_t n);
int receive_msg(int fd, char *buffer);
int find_empty_service();
int new_client(int fd, int client_id);
int delete_client(int client_id);
char **strtok_bychar(const char *cmds_in_line, const char delim, int *cmds_count);
void free_2Darray(char **array, int count);
int who(int client_id);
int name(int client_id, const char *str);
int tell(int client_id, int dst_id, const char *msg);
int yell(int client_id, const char *msg);
int rwg_service(int client_id, char *cmd);
int exec_shell_command(const char *command, int output_format, int client_id, int read_fd, int write_fd);
int read_to_new_line(int client_id);
int find_empty_entry(int client_id);
int find_name(char *name);

int main(int argc, char *argv[])
{
    int listen_fd = 0;
    fd_set active_read_fds, read_fd_set;
    int nfds = 0, i = 0;
    int activity = 0;
    int client_table[FD_SETSIZE] = {0};

    chroot(WORKPWD);
    chdir(WORKPWD);

    FD_ZERO(&active_read_fds);
    FD_ZERO(&read_fd_set);
    memset(client, 0, sizeof(User) * MAX_CLIENT);

    switch (argc)
    {
        case 1:
            listen_fd = passive_socket(RWG_PORT, "tcp", 0);
            break;
        case 2:
            listen_fd = passive_socket(argv[1], "tcp", 0);
            break;
        default:
            fprintf(stderr, "Usage: %s <port>\n", argv[0]);
            exit(1);
    }

    FD_SET(listen_fd, &active_read_fds);
    nfds = listen_fd + 1;

    while (1)
    {
        memcpy(&read_fd_set, &active_read_fds, sizeof read_fd_set);

        activity = select (nfds, &read_fd_set, NULL, NULL, NULL);
        if (activity < 0 && errno != EINTR)
        {
            perror ("select");
            exit (EXIT_FAILURE);
        }
        for (i = 0; i <  nfds; ++i)
        {
            char buffer[BUFFERSIZE] = {'\0'};
            if (FD_ISSET(i, &read_fd_set))
            {
                if (i == listen_fd)
                {
                    int conn_fd = wait_for_connection(listen_fd);
                    if (conn_fd < 0)
                    {
                        close(conn_fd);
                        break;
                    }
                    FD_SET(conn_fd, &active_read_fds);
                    if (conn_fd >= nfds) nfds = conn_fd + 1;

                    int client_id = find_empty_service();
                    client_table[conn_fd] = client_id;
                    new_client(conn_fd, client_id);
                    send_msg(client_id, PROMPT);
                }
                else
                {
                    if (read_line(i, buffer, BUFFERSIZE) <= 0 || rwg_service(client_table[i], buffer) == 1)
                    {
                        delete_client(client_table[i]);
                        client_table[i] = 0;
                        FD_CLR(i, &active_read_fds);
                    }
                    else
                        send_msg(client_table[i], PROMPT);
                }
            }
        }
    }
    close(listen_fd);
    return 0;
}
int send_msg(int client_id, const char *msg)
{
    int nbytes = 0;
    nbytes = write(client[client_id].sock_fd, msg, strlen(msg));
    if (nbytes < 0)
    {
        perror("write");
        exit(EXIT_FAILURE);
    }
    return nbytes;
}
int read_line(int fd, void *buffer, size_t n)
{
    ssize_t nbytes;
    size_t total = 0;
    char *buf;
    char ch;
    int end = 0;

    if (n <= 0 || buffer == NULL)
    {
        errno = EINVAL;
        return -1;
    }
    buf = buffer;

    while (end == 0)
    {
        nbytes = read(fd, &ch, 1);
        switch (nbytes)
        {
            case -1:
                if (errno != EINTR)
                    return -1;
                break;
            case 0:
                if (total == 0)
                    return 0;
                else
                    end = 1;
                break;
            case 1:
                if (ch == '\n')
                    end = 1;
                else if (ch == '\r')
                    continue;
                else if (total < n - 1)
                {
                    total++;
                    *buf++ = ch;
                }
                break;
            default:
                perror("read");
                return -1;
        }
    }
    *buf = '\0';
    return total;
}
void broadcast(const char *msg)
{
    int i = 0;
    for (i = 1; i < MAX_CLIENT; ++i)
    {
        if (client[i].sock_fd != 0)
            send_msg(i, msg);
    }
}
int find_empty_service()
{
    int i = 0;
    for (i = 1; i < MAX_CLIENT; ++i)
    {
        if (client[i].sock_fd == 0)
        {
            return i;
        }
    }
    return -1;
}
int new_client(int fd, int client_id)
{
    const char Welcome_message[3][42] =
    {
        {"****************************************\r\n"},
        {"** Welcome to the information server. **\r\n"},
        {"****************************************\r\n"}
    };
    char *msg = "*** User '(no name)' entered from CGILAB/511. ***\n";
    struct sockaddr_in sock;
    socklen_t len = sizeof(sock);

    write(fd, Welcome_message, strlen(*Welcome_message));

    client[client_id].sock_fd = fd;
    client[client_id].name = strdup("(no name)");
    memset(client[client_id].ip, '\0', INET_ADDR_LEN);
    memset(&sock, 0, sizeof(sock));
    if (getpeername(fd, (struct sockaddr *)&sock, &len) == -1)
        perror("getpeername");
    get_ip_str((struct sockaddr *)&sock, client[client_id].ip, INET_ADDR_LEN);
    client[client_id].port = ntohs(sock.sin_port);
    memset(client[client_id].env, '\0', BUFFERSIZE);
    strncpy(client[client_id].env, ENV_PATH, strlen(ENV_PATH));

    int i = 0;
    for (i = 1; i < MAX_CLIENT; ++i)
    {
        char fifo_path[50] = {'\0'};
        snprintf(fifo_path, 50, "%s/%d_to_%d.fifo", FIFO_PATH, i, client_id);
        if (mkfifo(fifo_path, PERMS) == -1)
        {
            if (errno != EEXIST)
                perror("can't create FIFO\n");
        }
        client[client_id].fifo_readfd[i] = open(fifo_path, O_RDONLY | O_NONBLOCK);
        memset(client[client_id].fifo_readcmd[i], '\0', CMDSIZE);
    }
    broadcast(msg);
    return 0;
}
int delete_client(int client_id)
{
    char msg[MAX_MSG_LEN] = {'\0'};
    snprintf(msg, MAX_MSG_LEN, "*** User '%s' left. ***\n", client[client_id].name);
    broadcast(msg);

    close(client[client_id].sock_fd);
    client[client_id].sock_fd = 0;
    free(client[client_id].name);
    memset(client[client_id].ip, '\0', INET_ADDR_LEN);
    client[client_id].port = 0;
    memset(client[client_id].env, '\0', BUFFERSIZE);
    memset(client[client_id].pipe_number_entry, 0, sizeof(client[client_id].pipe_number_entry));

    int i = 0;
    for (i = 1; i < MAX_CLIENT; ++i)
    {
        char fifo_path[50] = {'\0'};
        snprintf(fifo_path, 50, "%s/%d_to_%d.fifo", FIFO_PATH, i, client_id);
        close(client[client_id].fifo_readfd[i]);
        unlink(fifo_path);
        memset(client[client_id].fifo_readcmd[i], '\0', CMDSIZE);
        if (client[client_id].pipe_number_entry[i][0] != 0)
            close(client[client_id].pipe_number_entry[i][0]);
        client[client_id].pipe_number_entry[i][1] = 0;
    }
    return 0;
}
int who(int client_id)
{
    int i = 0;

    send_msg(client_id, "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n");
    for (i = 1; i < MAX_CLIENT; ++i)
    {
        if (client[i].sock_fd != 0)
        {
            char msg[MAX_MSG_LEN] = {'\0'};
            if (i == client_id)
                snprintf(msg, MAX_MSG_LEN, "%d\t%s\tCGILAB/511\t<- me\n", i, client[i].name);
            else
                snprintf(msg, MAX_MSG_LEN, "%d\t%s\tCGILAB/511\t\n", i, client[i].name);
            send_msg(client_id, msg);
        }
    }
    return 0;
}
int name(int client_id, const char *name)
{
    int i = 0;

    for (i = 1; i < MAX_CLIENT; ++i)
    {
        if (client[i].sock_fd != 0)
        {
            if (strcmp(client[i].name, name) == 0)
            {
                char err_msg[MAX_MSG_LEN] = {'\0'};
                snprintf(err_msg, MAX_MSG_LEN, "*** User '%s' already exists. ***\n", name);
                send_msg(client_id, err_msg);
                return -1;
            }
        }
    }
    free(client[client_id].name);
    client[client_id].name = strdup(name);
    char msg[MAX_MSG_LEN] = {'\0'};
    snprintf(msg, MAX_MSG_LEN, "*** User from CGILAB/511 is named '%s'. ***\n", client[client_id].name);
    broadcast(msg);

    return 0;
}
int tell(int client_id, int dst_id, const char *msg)
{
    char message[BUFFERSIZE] = {'\0'};

    if (client[dst_id].sock_fd == 0)
    {
        char err_msg[MAX_MSG_LEN] = {'\0'};
        snprintf(err_msg, MAX_MSG_LEN, "*** Error: user #%d does not exist yet. ***\n", dst_id);
        send_msg(client_id, err_msg);
        return -1;
    }
    snprintf(message, MAX_MSG_LEN, "*** %s told you ***: %s\n", client[client_id].name, msg);
    send_msg(dst_id, message);
    return 0;
}
int yell(int client_id, const char *msg)
{
    char message[BUFFERSIZE] = {'\0'};
    snprintf(message, MAX_MSG_LEN, "*** %s yelled ***: %s\n", client[client_id].name, msg);
    broadcast(message);
    return 0;
}
char **strtok_bychar(const char *cmds_in_line, const char delim, int *cmds_count)
{
    char **result = 0;
    char *tmp = strdup(cmds_in_line);
    char *cptr = tmp;
    char *token = 0;
    char *rest_of_cmd = NULL;
    int index = 0;
    int flag = 0;

    *cmds_count = 0;
    while (*cptr)
    {
        if ((*cptr != delim) && (flag == 0))
        {
            (*cmds_count)++;
            flag = 1;
        }
        else if ((*cptr == delim) && (flag == 1))
        {
            flag = 0;
        }
        cptr++;
    }
    while (result == NULL)
    {
        result = malloc(sizeof(char*) * ((*cmds_count) + 1));
    }
    token = strtok_r(tmp, &delim, &rest_of_cmd);

    for (index = 0; index < *cmds_count; index ++)
    {
        result[index] = strdup(token);
        token = strtok_r(rest_of_cmd, &delim, &rest_of_cmd); 
    }
    result[index] = 0;
    free(tmp);
    return result;
}
void free_2Darray(char **array, int count)
{
    int i = 0;
    for (i = 0; i < count; i++) {
        if (array[i]) free(array[i]);
    }
    free(array);
}
int rwg_service(int client_id, char *cmd)
{
    int cmd_count = 0;
    char **cmd_split = 0;
    int i = 0;
    char message[BUFFERSIZE] = {'\0'};
    int read_fd = read_to_new_line(client_id);
    int write_fd = client[client_id].sock_fd;

    cmd_split = strtok_bychar(cmd, ' ', &cmd_count);

    if (strncmp(cmd, "exit", 4) == 0) return 1;
    if (strncmp(cmd, "name", 4) == 0)
    {
        if (cmd_count > 1)
            name(client_id, cmd_split[1]);
    }
    else if (strncmp(cmd, "tell", 4) == 0)
    {
        int target_id;
        if (cmd_count > 2) {
            if (isdigit(cmd_split[1][0]))
                target_id = atoi(cmd_split[1]);
            else
                target_id = find_name(cmd_split[1]);

            for (i = 2; i < cmd_count; ++i)
            {
                strncat(message, cmd_split[i], strlen(cmd_split[i]));
                strncat(message, " ", 1);
            }
            tell(client_id, target_id, message);
        }
    }
    else if (strncmp(cmd, "yell", 4) == 0)
    {
        if (cmd_count > 1)
            yell(client_id, &cmd[5]);
    }
    else if (strncmp(cmd, "who", 3) == 0)
    {
        who(client_id);
    }
    else if (strncmp(cmd, "setenv", 6) == 0)
    {
        if (cmd_count > 2)
        {
            memset(client[client_id].env, '\0', BUFFERSIZE);
            strncpy(client[client_id].env, cmd_split[2], strlen(cmd_split[2]));
        }
    }
    else if (strncmp(cmd, "printenv", 8) == 0)
    {
        if (cmd_count > 1)
        {
            snprintf(message, BUFFERSIZE, "PATH=%s\n", client[client_id].env);
            send_msg(client_id, message);
        }
    }
    else
    {
        char command[CMDSIZE] = {'\0'};
        for (i = 0; i < cmd_count; i++)
        {
            switch(cmd_split[i][0])
            {
                case '|':
                    read_fd = exec_shell_command(command, 1, client_id, read_fd, write_fd);
                    memset(command, '\0', CMDSIZE);
                    if (strlen(cmd_split[i]) != 1)
                    {    
                        int index = find_empty_entry(client_id);
                        client[client_id].pipe_number_entry[index][0] = read_fd;
                        client[client_id].pipe_number_entry[index][1] = atoi(&cmd_split[i][1]);
                    }
                    break;
                case '!':
                    read_fd = exec_shell_command(command, 2, client_id, read_fd, write_fd);
                    int index = find_empty_entry(client_id);
                    client[client_id].pipe_number_entry[index][0] = read_fd;
                    client[client_id].pipe_number_entry[index][1] = atoi(&cmd_split[i][1]);
                    memset(command, '\0', CMDSIZE);
                    break;
                case '>':
                    if (strlen(cmd_split[i]) == 1)
                        write_fd = fileno(fopen(cmd_split[++i], "w"));
                    else
                    {
                        int target_id = atoi(&cmd_split[i][1]);
                        if (client[target_id].sock_fd == 0)
                        {
                            snprintf(message, MAX_MSG_LEN, "*** Error: user #%d does not exist yet. ***\n", target_id);
                            send_msg(client_id, message);
                            i = cmd_count;
                        }
                        else if (strlen(client[target_id].fifo_readcmd[client_id]) == 0)
                        {
                            char fifo_path[50] = {'\0'};
                            snprintf(fifo_path, 50, "%s/%d_to_%d.fifo", FIFO_PATH, client_id, target_id);
                            write_fd = open(fifo_path, O_WRONLY);
                            strncpy(client[target_id].fifo_readcmd[client_id], command, strlen(command));
                            snprintf(message, MAX_MSG_LEN, "*** %s (#%d) just piped '%s' to %s (#%d) ***\n", client[client_id].name, client_id, cmd, client[target_id].name, target_id);
                            broadcast(message);
                        }
                        else
                        {
                            snprintf(message, MAX_MSG_LEN, "*** Error: the pipe #%d->#%d already exists. ***\n", client_id, target_id);
                            send_msg(client_id, message);
                            i = cmd_count;
                        }
                    }
                    break;
                case '<':
                    if (strlen(cmd_split[i]) > 1)
                    {
                        int target_id = atoi(&cmd_split[i][1]);
                        if (client[target_id].sock_fd  == 0 || strlen(client[client_id].fifo_readcmd[target_id]) == 0)
                        {
                            snprintf(message, MAX_MSG_LEN, "*** Error: the pipe #%d->#%d does not exist yet. ***\n", target_id, client_id);
                            send_msg(client_id, message);
                            i = cmd_count;
                        }
                        else
                        {
                            read_fd = client[client_id].fifo_readfd[target_id];
                            memset(client[client_id].fifo_readcmd[target_id], '\0', CMDSIZE);
                            snprintf(message, MAX_MSG_LEN, "*** %s (#%d) just received from %s (#%d) by '%s' ***\n", client[client_id].name, client_id, client[target_id].name, target_id, cmd);
                            broadcast(message);
                        }
                    }
                    break;
                default:
                    strcat(command, cmd_split[i]);
                    strcat(command, " ");
                    break;
            }
            if (read_fd == -1) break;
        }
        if (i == cmd_count && strlen(command) > 0)
        {
            exec_shell_command(command, 0, client_id, read_fd, write_fd);
            memset(command, '\0', CMDSIZE);
        }
    }
    free_2Darray(cmd_split, cmd_count);
    return 0;
}
int exec_shell_command(const char *command, int output_format, int client_id, int read_fd, int write_fd)
{
    pid_t childpid = 0;
    int pipefd[2];
    int status = 0, flag = 0;
    int arg_count = 0;
    char **arg_split = strtok_bychar(command, ' ', &arg_count);

    printf("%s\n", arg_split[0]);

    if (pipe(pipefd) == -1) {
        perror("ERROR on pipe.\r\n");
    }
    if ((childpid = fork()) < 0) {
        perror("ERROR on fork.\r\n");
        exit(-1);
    } else if (childpid == 0) {
        char path[BUFFERSIZE] = {'\0'};
        snprintf(path, BUFFERSIZE, "PATH=%s", client[client_id].env);
        putenv(path);

        close(pipefd[0]);
        if (read_fd != STDIN)
        {
            flag = 1;
            dup2(read_fd, STDIN);
        }
        dup2(client[client_id].sock_fd, STDERR);
        switch (output_format)
        {
            case 0:
                dup2(write_fd, STDOUT);
                break;
            case 1:
                dup2(pipefd[1], STDOUT);
                break;
            case 2:
                dup2(pipefd[1], STDOUT);
                dup2(pipefd[1], STDERR);
                break;
            default:
                dup2(write_fd, STDOUT);
                break;
        }
        execvp(arg_split[0], &arg_split[0]);
        char err_msg[BUFFERSIZE] = {'\0'};
        snprintf(err_msg, CMDSIZE, "Unknown command: [%s].\n", arg_split[0]);
        send_msg(client_id, err_msg);
        exit(EXIT_FAILURE);
    } else {
        do {    
            waitpid(childpid, &status, WUNTRACED | WCONTINUED);
        } while(!WIFEXITED(status) && !WIFSIGNALED(status));

        if (output_format == 0 || output_format == 3)
        {
            close(pipefd[0]);
            pipefd[0] = 0;
        }
        close(pipefd[1]);
    }
    free_2Darray(arg_split, arg_count);
    if (write_fd != client[client_id].sock_fd) close(write_fd);
    if (flag == 1) close(read_fd);
    if (WEXITSTATUS(status) == EXIT_FAILURE) return -1;
    return pipefd[0];
}
int find_empty_entry(int client_id)
{
    int i;
    for (i = 0; i < 1000; ++i) {
        if (client[client_id].pipe_number_entry[i][1] == 0)
            return i;
    }
    return -1;
}
int read_to_new_line(int client_id)
{
    int i = 0, flag = 0;
    int pipefd[2];
    char buffer[BUFFERSIZE] = {'\0'};

    if ((pipe(pipefd)) == -1)
        perror("ERROR on pipe.\n");                

    for (i = 0; i < 1000; ++i)
    {
        if (client[client_id].pipe_number_entry[i][1] > 0)
        {
            if (client[client_id].pipe_number_entry[i][1] == 1)
            {
                flag = 1;
                read(client[client_id].pipe_number_entry[i][0], buffer, BUFFERSIZE);
                close(client[client_id].pipe_number_entry[i][0]);
                client[client_id].pipe_number_entry[i][0] = 0;
                write(pipefd[1], buffer, strlen(buffer));
            }
            client[client_id].pipe_number_entry[i][1]--;
        }
        memset(buffer, '\0', BUFFERSIZE);
    }
    close(pipefd[1]);
    if (flag == 0)
    {
        close(pipefd[0]);
        return STDIN;
    }
    return pipefd[0];
}
int find_name(char *name)
{
    int i = 0;
    for (i = 1; i < MAX_CLIENT; ++i)
    {
        if (strcmp(client[i].name, name) == 0)
            return i;
    }
    return 0;
}
